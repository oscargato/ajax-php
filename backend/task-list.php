<?PHP
	include('db.php');
		$query = "SELECT * FROM task";
		$result = mysqli_query($connection, $query);

		if($result){
			$json = array();
			while($row = mysqli_fetch_array($result)){
				$json[] = array(
					'id' => $row['id'],
					'name' => $row['name'],
					'description' => $row['description'],
				);
			}
			$jsonstring = json_encode($json);
			echo $jsonstring;			
		}else{
			die('Query Failed'.mysqli_error($connection));
		}
?>