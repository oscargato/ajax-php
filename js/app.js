$(document).ready(function(){

	let edit = false;

	//************* Buscar ************* 
	$('#task-result').hide();
	$('#search').keyup(function(event){
		if($('#search').val()){
			let valor = $('#search').val();
			$.ajax({
				url: 'backend/task-search.php',
				type: 'POST',
				data: { valor },
				success: function(response){
					let template = '';
					let tasks = JSON.parse(response);
					tasks.forEach(task=>{
						template += `<li>${task.name}</li>`
					});
					$('#cont').html(template);
					$('#task-result').show();
				},
				error: function(response){
					console.log('Error!!!',response);
				},
			});
		}
	});
	//************* Buscar *************


	//************* Guardar Tarea *************
	$('#task-form').submit(function(event){
		const dataSave = {
			id: $('#taskID').val(),
			name: $('#name').val(),
			description: $('#description').val(),
		};

		let url;
		if(edit){
			url = 'backend/task-edit.php';
		}else{
			url = 'backend/task-add.php';		
		}

		console.log(dataSave);

		$.post(url, dataSave, function(response){
			fetchTasks();
			$('#task-form').trigger('reset');
		});			
		event.preventDefault();
	});
	//************* Guardar Tarea *************


	//************* Eliminar Tarea *************
	$(document).on('click','.task-delete', function(){
		if(confirm('Esta seguro de Eliminar')){
			let element = $(this)[0].parentElement.parentElement;
			let id = $(element).attr('taskID');
			$.post('backend/task-delete.php',{id},function(response){
				fetchTasks();
			});
		}	
	})	
	//************* Eliminar Tarea *************


	//************* Editar Tarea *************
	$(document).on('click','.task-item', function(){
		let element = $(this)[0].parentElement.parentElement;
		let id = $(element).attr('taskID');
		$.post('backend/task-single.php',{id},function(response){
			const task = JSON.parse(response);
			$('#name').val(task.name);
			$('#description').val(task.description);
			$('#taskID').val(task.id);
			edit = true;
		});
	})	
	//************* Editar Tarea *************


	//************* Listar Tareas *************
	fetchTasks();

	function fetchTasks(){
		$.ajax({
			url: 'backend/task-list.php',
			type: 'GET',
			success: function(response){
				let template = '';
				let tasks = JSON.parse(response);
				
				tasks.forEach(task=>{
					template += `
					<tr taskID="${task.id}">
						<td>${task.id}</td>
						<td><a href="#" class="task-item">${task.name}</a></td>
						<td>${task.description}</td>
						<td><button class="task-delete btn btn-danger">Delete</button></td>
					</tr>`
				});
				$('#tasks').html(template);			
			},
			error: function(response){
				console.log('Error!!!',response);
			},
		});	
	}
	//************* Listar Tareas *************		
});